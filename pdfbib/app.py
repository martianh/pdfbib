import os
from flask import Flask, render_template, request, redirect, url_for, flash, send_from_directory

from werkzeug.utils import secure_filename
from datetime import datetime

from flask_debugtoolbar import DebugToolbarExtension

from models import db, Book, User

from flaskext.markdown import Markdown


UPLOAD_FOLDER = "uploads/"
ALLOWED_EXTENSIONS = {'txt', 'pdf', 'doc', 'docx', 'odt', 'epub', 'djvu'}


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///pdfbib.db'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.secret_key = b'_5#y2L"F4Q8z\n\asdfasd]/'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

Markdown(app)

db.init_app(app)

with app.app_context():
    db.create_all()

app.debug = True
toolbar = DebugToolbarExtension(app)

# @app.before_first_request
# def create_table():
#     db.create_all()
    
def init_db():
    db.drop_all()
    db.create_all()


def allowed_file(filename):
    return '.' in filename and \
        filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def save_file(file):
    """save a file from a request"""
    filename = secure_filename(file.filename)
    print(f"we will save {filename}.")
    file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
    print(f"file {filename} saved.")

def read_markdown(filename):
    if not os.path.isfile(filename):
        print('File does not exist.')
    else:
        with open(filename) as md:
            return md.read()

@app.route('/', methods=['GET', 'POST'])
def index():
    header = read_markdown('header.md')
    footer = read_markdown('footer.md')

    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file found in request')
            return redirect(request.url)

        f = request.files['file']
        filename = secure_filename(f.filename)
        print(f)
        
        # No file selected:
        if f.filename == '':
            flash('No selected file')
            print('No file selected')
            return redirect(request.url)

        # add a book
        if f and allowed_file(f.filename):
            # save upload:
            save_file(f)

            # save data to db:
            name = request.form['name']
            author = request.form['author']
            publisher = request.form['publisher']
            date = request.form['date']
            new_stuff = Book(name=name, author=author, filename=filename, publisher=publisher, date=date)

            try:
                db.session.add(new_stuff)
                db.session.commit()
                return redirect('/')
            except:
                return "There was a problem adding new books."
        else:
            flash('file type not allowed')
            print('file extension not allowed.')
            return redirect(request.url)

    else:
        books = Book.query.order_by(Book.created_at).all()
        return render_template('index.html', books=books, header=header, footer=footer, extensions=ALLOWED_EXTENSIONS, action="add", endpoint="/")


@app.route('/delete/<int:id>')
def delete(id):
    book = Book.query.get_or_404(id)
    # TODO: make it mandatory to delete both or neither
    # remove item from db:
    try:
        db.session.delete(book)
        db.session.commit()
        print(f"{book.filename} removed from db.")
        flash(f'{book.filename} removed.')
        os.remove(os.path.join(app.config['UPLOAD_FOLDER'], book.filename))
        print(f"{book.filename} file removed from uploads.")
        return redirect('/')
    except:
        return "There was a problem deleting the item."


@app.route('/update/<int:id>', methods=['GET', 'POST'])
def update(id):
    book = Book.query.get_or_404(id)

    if request.method == 'POST':
        book.name = request.form['name']
        book.author = request.form['author']
        book.publisher = request.form['publisher']
        book.date = request.form['date']

        f = request.files['file']
        book.filename = secure_filename(f.filename)

        if f and allowed_file(f.filename):
            # save upload:
            save_file(f)

        try:
            db.session.commit()
            flash(f'item {book.name} updated.')
            return redirect('/')
        except:
                return "There was a problem updating the item."

    else:
        title = "Updating item"
        endpoint = f"/update/{id}"
        return render_template('update.html', title=title, book=book, action="update", endpoint=endpoint)

# actually view the file:
@app.route('/uploads/<name>')
def download_file(name):
    return send_from_directory(app.config["UPLOAD_FOLDER"], name)

if __name__ == '__main__':
    app.run(debug=True)
